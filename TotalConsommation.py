import psutil
import wmi
import time
import matplotlib.pyplot as plt
import datetime

POWER_PER_8GB = 3 
conso_ecran = 30 
cons_harddrive=0.005 
TDP = 158.75
cons_reseau=5.12
cons_co2_2022=60 #g par kWh

time_list = []
energy_list = []
total_energy = 0 # consommation totale d'�nergie
Sum_energy = 0 #  somme des �nergies
conso_co2=0 
conso_co2_total=0
start_time = time.time() # temps de d�part
last_update_time = start_time # dernier temps d'affichage de la consommation moyenne
avg_energy_list = [] # liste pour stocker la consommation moyenne par heure

fig, ax = plt.subplots()
ax.set_xlabel("Temps (s)")
ax.set_ylabel("co2 emis en mg")
ax.set_title("Emission de co2")

while True:

    #T1 :
    time1 = time.time()
    
    #CPU
    num_cores = psutil.cpu_count(logical=False)
    cpu_usage1 = psutil.cpu_percent()
    power_cpu1 = (TDP*cpu_usage1/100)

    #RAM
    mem_usage1 = psutil.virtual_memory().used
    power_ram1 = (mem_usage1 / (8 * 2**30)) * POWER_PER_8GB

    #Reseau
    net_io_counters1 = psutil.net_io_counters()

    #HardDiskDrive
    disk_io1 = psutil.disk_io_counters(perdisk=True)

    #Luminosity
    wmi_obj1 = wmi.WMI(namespace="wmi")
    brightness1 = wmi_obj1.WmiMonitorBrightness()[0].CurrentBrightness
    coefficient1 = brightness1 / 100
    power_lum1 = conso_ecran * coefficient1

    plt.pause(1)

    #T2
    time2 = time.time()
    delta_t = time2-time1

    #CPU
    cpu_usage2 = psutil.cpu_percent()
    power_cpu2 = (TDP*cpu_usage2/100)
    average_power_cpu = (power_cpu1 + power_cpu2)/2
    energy_cpu = (average_power_cpu*delta_t)/3600

    #RAM
    mem_usage2 = psutil.virtual_memory().used
    power_ram2 = (mem_usage2 / (8 * 2**30)) * POWER_PER_8GB
    average_power_ram = (power_ram1 + power_ram2)/2
    energy_ram = (average_power_ram*delta_t)/3600

    #Reseau
    net_io_counters2 = psutil.net_io_counters()
    bytes_sent = net_io_counters2.bytes_sent-net_io_counters1.bytes_sent
    bytes_recv = net_io_counters2.bytes_recv-net_io_counters1.bytes_recv
    total_bytes = bytes_sent + bytes_recv
    if total_bytes != 0:
        energy_reseau= cons_reseau * 1000 * (total_bytes / 2**30 )
    else:
        energy_reseau = 0

    #HardDiskDrive
    disk_io2 = psutil.disk_io_counters(perdisk=True)
    energy_hardDrive1 = sum(disk_io1[disk].read_bytes + disk_io1[disk].write_bytes for disk in disk_io1) / 2**30 *cons_harddrive * 1000
    energy_hardDrive2 = sum(disk_io2[disk].read_bytes + disk_io2[disk].write_bytes for disk in disk_io2) / 2**30 *cons_harddrive * 1000
    energy_hardDrive =energy_hardDrive2-energy_hardDrive1

    #Luminosity
    wmi_obj2 = wmi.WMI(namespace="wmi")
    brightness2 = wmi_obj2.WmiMonitorBrightness()[0].CurrentBrightness
    coefficient2 = brightness2 / 100
    power_lum2 = conso_ecran * coefficient2
    average_power_lum = (power_lum1 + power_lum2)/2
    energy_lum = (average_power_lum*delta_t)/3600

    #Somme des energies consomm�es en Wh
    Sum_energy = energy_cpu+energy_hardDrive+energy_lum+energy_ram+energy_reseau
 
    # Calcul de la consommation totale d'�nergie 
    total_energy+=Sum_energy

    # Calcul de la consommation de co2 en mg de co2
    conso_co2=Sum_energy*cons_co2_2022
    conso_co2_total+=conso_co2
    
    # Ajout des donn�es aux listes
    heure_actuelle = datetime.datetime.now().strftime("%H:%M:%S")
    time_list.append(len(time_list))
    energy_list.append(conso_co2)
 
    # Mise � jour du graphique
    ax.plot(time_list, energy_list)

    # Calcul de la consommation moyenne par minute et affichage toutes les minutes
    elapsed_time = time.time() - start_time
    if time.time() - last_update_time >= 60:
        last_update_time = time.time()
        avg_energy = total_energy /elapsed_time*60
        avg_energy_list.append(avg_energy)
        print(f"Temps: {heure_actuelle}s\tConsommation d'energie: {Sum_energy:.2f}Wh\tConsommation moyenne par minute: {avg_energy:.2f}Wh, vous avez emis au total {conso_co2_total:.2f} mg de co2")
        
    # Affichage de la consommation d'�nergie toutes les secondes en Ws
    else:
        print(f"Temps : {heure_actuelle}s\tConsommation d'energie : {Sum_energy*3600:.2f} Ws, cela equivaut a {conso_co2:.2f} mg de co2 emis")
        print(f"Temps : {heure_actuelle}s\tcpu : {energy_cpu*3600:.2f} Ws")
        print(f"Temps : {heure_actuelle}s\tram : {energy_ram*3600:.2f} Ws")
        print(f"Temps : {heure_actuelle}s\tband passante : {energy_reseau*3600:.2f} Ws")
        print(f"Temps : {heure_actuelle}s\tluminosity : {energy_lum*3600:.2f} Ws")
        print(f"Temps : {heure_actuelle}s\tharddrive : {energy_hardDrive*3600:.2f} Ws")

    
    
    